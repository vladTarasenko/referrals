import React from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import WelcomePage from './scenes/WelcomePage/WelcomePage';
import ExplanationPage from './scenes/ExplanationPage/ExplanationPage';
import WaitingList from './scenes/WaitingList/WaitingList';
import ShareInvites from './scenes/ShareInvites/ShareInvites';
import * as actions from './services/redux/actions';

function createReduxHandlers(dispatch) {
  // COOKIES
  const clearCurrSessionCookies = () => {
    dispatch(actions.clearCurrSessionCookies());
  };
  const createNewCookie = (data) => {
    dispatch(actions.createNewCookie(data));
  };
  const saveCurrSessionCookies = () => {
    dispatch(actions.saveCurrSessionCookies());
  };
  // Counting the amount of people before user
  return {
    cookieHandlers: {
      clearCurrSessionCookies,
      createNewCookie,
      saveCurrSessionCookies,
    },
  };
}

function mapStateToProps(state) {
  return {
    model: state.model,
    configuration: state.configuration,
  };
}

class App extends React.Component {
  constructor(props) {
    super(props);

    // const { pathname } = this.props.location;
    const pathname = '/';
    this.state = {
      routes: {
        pathToWelcomePage: pathname,
        pathToExplanationPage: `${pathname}explanation-page`,
        pathToWaitingList: `${pathname}waiting-list`,
        pathToShareInvites: `${pathname}share-invites`,
        pathToLoginPage: `${pathname}waiting-list`,
        // wairing list goes after login
        // `${pathname}login`, // not implemented
        pathToSignUpPage: `${pathname}waiting-list`,
        // waiting list goes after login
        // `${pathname}sign-up`, // not implemented
        pathToSomeThankYouPage: `${pathname}thank-you`, // not implenented
      },
    };

    this.reduxHandlers = createReduxHandlers(this.props.dispatch);
  }
  render() {
    return (
      <Switch>
        <Route
          exact
          path={this.state.routes.pathToWelcomePage}
          render={() => (
            <WelcomePage
              configuration={this.props.configuration}
              routes={this.state.routes}
              historyPush={this.props.history.push}
              reduxHandlers={this.reduxHandlers.cookieHandlers}
            />
          )}
        />
        <Route
          path={this.state.routes.pathToExplanationPage}
          render={() => (
            <ExplanationPage
              routes={this.state.routes}
            />
          )}
        />
        <Route
          path={this.state.routes.pathToWaitingList}
          render={() => (
            <WaitingList
              user={this.props.model.userInfo}
              routes={this.state.routes}
            />
          )}
        />
        <Route
          path={this.state.routes.pathToShareInvites}
          render={() => (
            <ShareInvites
              goBack={this.props.history.goBack}
              user={this.props.model.userInfo}
            />
          )}
        />
      </Switch>
    );
  }
}

export default withRouter(connect(mapStateToProps)(App));

