const model = {
  currSessionCookies: {},
  userInfo: {
    invitationLink: 'https://goo.gl/DdQlou',
    usersRegisteredByThisLink: 0,
    ID: Math.round((Math.random() * 10000) + 382319),
  },
};

export default model;
