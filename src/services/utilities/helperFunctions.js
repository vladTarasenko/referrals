export function generateRandomKey() {
  return Math.random().toString(36).substr(2, 5);
}

export function formatNumberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

export function saveCurrentSessionCookies(cookiesHash) {
  const keys = Object.keys(cookiesHash);

  keys.forEach((key) => {
    document.cookie = `${key}=${cookiesHash[key]};`;
  });
}

export function shareLink(url) {
  if (navigator.share) {
    navigator.share({
      title: 'Share Geo Referral Invitation Link',
      text: 'The referrals of the future geo referral is coming! Get early acess with my link:',
      url,
    })
      .then(() => console.log('Successful share'))
      .catch(error => console.warn('Error sharing', error));
  } else {
    console.warn('navigator.share is not defined');
  }
}

export function getQueueLength(user) {
  if (!user) {
    return 'oo'; // infinity sign
  }
  const offset = 382319;
  let queueLength =
    (user.ID - offset) - ((Number(user.usersRegisteredByThisLink) || 0) * 4000);
  if (!queueLength) {
    queueLength = 'oo';
  } else if (queueLength < 0) {
    queueLength = 0;
  } else {
    queueLength = formatNumberWithCommas(queueLength);
  }
  return queueLength;
}

export function runScriptForGreenVisitor() {
  console.log('script for green visitor is running');
}

export function runScriptForRedVisitor() {
  console.log('script for red visitor is running');
}
