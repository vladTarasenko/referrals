import * as helpers from './helperFunctions';

describe('helper utilities', () => {
  describe('helper function: generateRandomKey', () => {
    const { generateRandomKey } = helpers;
    it('output of generateRandomKey should be a string', () => {
      expect(typeof generateRandomKey()).toEqual('string');
    });
    it('ouput of generatedRandomKey should be of length 5', () => {
      expect(generateRandomKey().length).toEqual(5);
    });  
  });
  
  describe('helper function: formatNumberWithCommas', () => {
    const { formatNumberWithCommas } = helpers;
    it('should format value of 9000 to 9,000', () => {
      expect(formatNumberWithCommas(9000)).toEqual('9,000');
    });
    it('should format value of 9012345 to 9,012,345', () => {
      expect(formatNumberWithCommas(9012345)).toEqual('9,012,345');
    });
    it('should not format value of 900', () => {
      expect(formatNumberWithCommas(900)).toEqual('900');
    });
  });

  describe('helper funtion: getQueueLength', () => {
    const { getQueueLength } = helpers;
    it('should deal with undefined value passed in', () => {
      expect(getQueueLength(undefined)).toEqual('oo');
    });

    it('should deal with empty object passed in', () => {
      expect(getQueueLength({})).toEqual('oo');
    })

    it("should deal with the case where 'usersRegisteredByThisLink' property of passed object is missing", () => {
      expect(getQueueLength({
        ID: 400000,
      }))
        .toEqual(helpers.formatNumberWithCommas(400000 - 382319));
    });

    it("should deal with the case where 'usersRegisteredByThisLink' property of passed object is provided", () => {
      expect(getQueueLength({
        ID: 400000,
        usersRegisteredByThisLink: 1,
      }))
        .toEqual(helpers.formatNumberWithCommas((400000 - 382319) - (1 * 4000)));
    });

    it('should deal with case where queue length is negative', () => {
      expect(getQueueLength({
        ID: 382319,
        usersRegisteredByThisLink: 1,
      }))
        .toEqual(0);
    });
  });
});
