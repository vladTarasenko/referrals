import configuration from '../appState/configuration.json';
import model from '../appState/model';
import { saveCurrentSessionCookies } from '../utilities/helperFunctions';

const initialState = {
  model: JSON.parse(JSON.stringify(model)),
  configuration: JSON.parse(JSON.stringify(configuration)),
};

export default function reducers(state = initialState, action) {
  const newState = JSON.parse(JSON.stringify(state));

  switch (action.type) {
    case 'CLEAR_CURR_SESSION_COOKIES':
      newState.model.currSessionCookies = {};
      break;
    case 'SAVE_CURR_SESSION_COOKIES':
      saveCurrentSessionCookies(newState.model.currSessionCookies);
      break;
    case 'CREATE_NEW_COOKIE':
      newState.model.currSessionCookies[action.key] = action.value;
      break;
    default:
      console.log('DEFAULT');
  }

  return newState;
}
