import reducer from './reducer';
import * as types from './actions';
import configuration from '../appState/configuration.json';
import model from '../appState/model';

const initialState = {
  model: JSON.parse(JSON.stringify(model)),
  configuration: JSON.parse(JSON.stringify(configuration)),
};

describe('cookies reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should return state with modified model.currSessionCookies', () => {
    expect(reducer(
      undefined,
      types.createNewCookie({
        key: 'test-cookie',
        value: 'created',
      }),
    ).model.currSessionCookies)
      .toEqual({ 'test-cookie': 'created' });
  });

  it("should write { 'test-cookie': 'created' } to document.cookie", () => {
    const prevState = reducer(undefined, types.createNewCookie({
      key: 'test-cookie',
      value: 'created',
    }));
    reducer(prevState, types.saveCurrSessionCookies());
    expect(document.cookie).toEqual('test-cookie=created');
  });

  it('should clear model.currSessionCookies', () => {
    const prevState = reducer(undefined, types.createNewCookie({
      key: 'test-cookie',
      value: 'created',
    }));    
    expect(reducer(prevState, types.clearCurrSessionCookies()).model.currSessionCookies)
      .toEqual({});
  });
});