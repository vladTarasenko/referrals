export function clearCurrSessionCookies() {
  return {
    type: 'CLEAR_CURR_SESSION_COOKIES',
  };
}

export function saveCurrSessionCookies() {
  return {
    type: 'SAVE_CURR_SESSION_COOKIES',
  };
}

export function createNewCookie({ key, value }) {
  return {
    type: 'CREATE_NEW_COOKIE',
    key,
    value,
  };
}

export function shareLink(url) {
  return {
    type: 'SHARE_LINK',
    url,
  };
}
