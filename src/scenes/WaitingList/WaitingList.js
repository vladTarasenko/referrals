import React from 'react';
import { Link } from 'react-router-dom';

import waitingManPicUrl from './waiting-man.png';
import './WaitingList.css';

import { getQueueLength } from '../../services/utilities/helperFunctions';

export default function WaitingList(props) {
  const peopleInQueueBeforeYou = getQueueLength(props.user);
  return (
    <section className="page-layout WaitingList">
      <div className="page-wrapper">
        <figure className="WaitingList__image">
          <img
            className="image"
            src={waitingManPicUrl}
            alt="man sitting on the bench"
          />
        </figure>

        <div className="WaitingList__queue">
          <div className="Stats">
            <p className="Stats__line Stats__line--highlighted">{peopleInQueueBeforeYou}</p>
            <p className="Stats__line">People ahead of you</p>
          </div>
          <div className="Stats">
            <p className="Stats__line Stats__line--highlighted">no one</p>
            <p className="Stats__line">Behind</p>
          </div>
        </div>
      </div>
      
      <p className="paragraph WaitingList__paragraph">
        Demand for Geo Refferals has been incredible and
        we&apos;re generating referrals as fast as we can.
      </p>

      <div className="link-group WaitingList__link-group">
        <Link className="link" to={props.routes.pathToShareInvites}>
          <button
            className="button WaitingList__button button--fullwidth"
          >
            Bump Me up the Queue
          </button>
        </Link>
        <Link className="link" to={props.routes.pathToSomeThankYouPage}>
          <button
            className="button WaitingList__button button--fullwidth button--dark"
          >
            Notify Me When My First Referral Is Ready
          </button>
        </Link>
      </div>
    </section>
  );
}
