import React from 'react';
import { Link } from 'react-router-dom';
import envelope from './envelope.png';
import './ShareInvites.css';
import { shareLink } from '../../services/utilities/helperFunctions';

export default function ShareInvites(props) {
  const { invitationLink } = props.user;
  return (
    <section className="page-layout ShareInvites">
      <div className="page-wrapper">
        <div className="ShareInvites__icon-wrapper">
          <span
            tabIndex="0"
            role="button"
            className="ShareInvites__icon"
            onClick={() => props.goBack()}
            onTouchStart={() => props.goBack()}
            onKeyUp={(evt) => {
              const keyCode = evt.keyCode || evt.which;
              if (keyCode === 13) { // enter key was pressed
                props.goBack();
              }
            }}
          >
            ×
          </span>
        </div>

        <figure className="ShareInvites__image">
          <img
            className="image"
            src={envelope}
            alt="opened envelope"
          />
        </figure>

        <p className="paragraph">This is your personal invitation link</p>
        <Link
          to={invitationLink}
          className="link link--highlight"
        >
          {invitationLink}
        </Link>
        <p className="paragraph">
          For every friend who sign up,
          we&apos;ll move you up the queue by at least 4000 places.
        </p>
      </div>

      <div className="ShareInvites__button">
        <button
          onClick={() => shareLink(invitationLink)}
          className="button button--fullwidth"
        >
          Share Invites
        </button>
      </div>
    </section>
  );
}
