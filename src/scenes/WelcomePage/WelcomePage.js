import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './WelcomePage.css';
import phonePicUrl from './phone-pic.png';

import PopUp from './components/PopUp/PopUp';
import QuestionsModal from './components/QuestionsModal/QuestionsModal';

export default class WelcomePage extends Component {
  constructor() {
    super();
    this.state = {
      shouldQuestionsPopUpDisplay: false,
    };
    this.togglePopUp = this.togglePopUp.bind(this);
  }
  togglePopUp() {
    this.setState(prevState => ({
      shouldQuestionsPopUpDisplay: !prevState.shouldQuestionsPopUpDisplay,
    }));
  }
  render() {
    return (
      <section className="page-layout WelcomePage" style={{ position: 'relative' }}>
        <div className="page-wrapper" style={{ marginTop: '30px' }}>
          <header className="WelcomePage__header">
            <h1>Help us build the referral source you want to use</h1>
          </header>
          <figure className="WelcomePage__image">
            <img
              className="image"
              src={phonePicUrl}
              alt="phone drawing"
            />
          </figure>
        </div>
        <div className="link-group WelcomePage__link-group">
          <Link
            onClick={this.togglePopUp}
            className="link link--shifted-down"
            to={this.props.routes.pathToWelcomePage}
          >
            <button className="button button--fullwidth">Continue</button>
          </Link>
          <Link
            to={this.props.routes.pathToLoginPage}
            className="link WelcomePage__link"
          >
            Already have an account? Log in
          </Link>
        </div>
        <PopUp shouldComponentDisplay={this.state.shouldQuestionsPopUpDisplay}>
          <QuestionsModal
            historyPush={this.props.historyPush}
            pathToExplanationPage={this.props.routes.pathToExplanationPage}
            shouldComponentDisplay={this.state.shouldQuestionsPopUpDisplay}
            reduxHandlers={this.props.reduxHandlers}
            configuration={this.props.configuration}
            togglePopUp={this.togglePopUp}
          />
        </PopUp>
      </section>
    );
  }
}
