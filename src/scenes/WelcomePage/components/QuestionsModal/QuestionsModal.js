import React, { Component } from 'react';
import './QuestionsModal.css';

import {
  generateRandomKey,
  runScriptForGreenVisitor,
  runScriptForRedVisitor,
} from '../../../../services/utilities/helperFunctions';

export default class QuestionsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currNodeIndex: this.props.configuration.treeRoot,
    };
    this.exitModal = this.exitModal.bind(this);
    this.submitAnswer = this.submitAnswer.bind(this);
  }
  componentDidMount() {
    window.addEventListener('keyup', () => {
      // user presses escape key on the keyboard
      const { keyCode } = window.event;
      if (keyCode === 27) {
        this.exitModal();
      }
    });
  }
  onSuccess() {
    // set expiration date of the cookie
    const date = new Date();
    date.setMonth(date.getMonth() + 1);
    this.props.reduxHandlers.createNewCookie({
      key: 'expires',
      value: date.toUTCString(),
    });

    this.props.reduxHandlers.saveCurrSessionCookies();
    this.props.historyPush(this.props.pathToExplanationPage);
  }
  onFail() {
    this.props.reduxHandlers.clearCurrSessionCookies();
  }
  submitAnswer(question, answer, nextNodeIndex, result) {
    this.props.reduxHandlers.createNewCookie({
      key: question,
      value: answer,
    });

    if (nextNodeIndex && !result) {
      // navigate to the next question(tree node)
      this.setState({
        currNodeIndex: nextNodeIndex,
      });
    } else if (!nextNodeIndex && result) {
      // we are finished traversing the tree
      this.props.reduxHandlers.createNewCookie({
        key: 'userStatus',
        value: result,
      });

      this.exitModal(null, true);

      // TODO put test scripts here!
      if (result === 'green') { // Remarket
        // run green script
        runScriptForGreenVisitor();
      } else if (result === 'red') { // Don't Remarket
        // run red script
        runScriptForRedVisitor();
      }
    }
  }
  exitModal(evt, shouldCookiesBeSaved) {
    // question process in aborted
    // we should delete cookies that were written this session
    const keyCode = evt ? (evt.keyCode || evt.which) : null;
    // keyCode is undefined if onCick or onTouchStart are fired
    // or exitModal is called from somewhere else
    // keyCode is 13 if onKeyUp is fired
    if (
      (!keyCode || keyCode === 13)
      && this.props.shouldComponentDisplay
    ) { // 13 represent enter
      this.props.togglePopUp();
      if (shouldCookiesBeSaved) {
        this.onSuccess();
      } else {
        this.onFail();
      }
      // we set current index(pointer) to the root
      // so we can start traversing question tree again next time
      this.setState({
        currNodeIndex: this.props.configuration.treeRoot,
      });
    }
  }
  render() {
    const configNode = this.props.configuration.tree[this.state.currNodeIndex];
    return (
      <section className="QuestionsModal">
        <div style={{ flex: 1 }}>
          <div className="QuestionsModal__icon-wrapper">
            <span
              className="QuestionsModal__icon"
              tabIndex="0"
              role="button"
              onClick={this.exitModal}
              onTouchStart={this.exitModal}
              onKeyUp={this.exitModal}
            >
              ×
            </span>
          </div>
          <div className="message-wrapper">
            <h5 className="QuestionsModal__message">Please share a little more info</h5>
          </div>
          <h2 className="QuestionsModal__question">{configNode.question}</h2>
        </div>
        <div className="QuestionsModal__answers">
          {configNode.options instanceof Array ?
            configNode.options.map(option => (
              <button
                key={generateRandomKey()}
                onClick={() =>
                  this.submitAnswer(
                    configNode.question,
                    option.answer,
                    option.nextNodeIndex,
                    option.result,
                  )
                }
                className="button button--dark-blue button--fullwidth button--sm-border-radius button--shifted-down"
              >
                {option.answer}
              </button>
            ))
            :
            <p style={{ color: 'red' }}>
              Something went wrong at QuestionsModal component
            </p>
          }
        </div>
      </section>
    );
  }
}
