import React from 'react';
import './PopUp.css';

export default function PopUp(props) {
  return (
    <div className={`PopUp ${props.shouldComponentDisplay ? '' : 'PopUp--hidden'}`}>
      <div className="PopUp__cover" />
      <div className="PopUp__content">
        {props.children}
      </div>
    </div>
  );
}
