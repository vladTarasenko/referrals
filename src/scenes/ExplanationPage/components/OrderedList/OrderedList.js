import React from 'react';
import './OrderedList.css';
import { generateRandomKey } from '../../../../services/utilities/helperFunctions';

export default function OrderedList(props) {
  const { list } = props;
  return (
    <ol className="OrderedList">
      {(list && list instanceof Array) ?
          list.map((item, idx) => (
            <li
              key={generateRandomKey()}
              className="OrderedList__item"
            >
              <div className="bulletPoint">
                <span className="bulletPoint__content">{idx + 1}</span>
              </div>
              <p className="paragraph OrderedList__paragraph">{item}</p>
            </li>
          ))
          :
          null
      }
    </ol>
  );
}
