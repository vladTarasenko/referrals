import React from 'react';
import { Link } from 'react-router-dom';
import './ExplanationPage.css';
import OrderedList from './components/OrderedList/OrderedList';

export default function ExplanationPage(props) {
  const explanations = [
    "There's been incredible demand for Geo Referral.\nWe're generating referrals as fast as we can but there's still a bit of a waiting list",
    "Once your first referral is ready, we'll ask you to accept or decline a prospect.\nIf you accept the referral, you pay a referral fee of 20-25% if/when the deal is closed.",    
  ];
  return (
    <section className="page-layout ExplanationPage">
      <div className="page-wrapper">
        <h2 className="ExplanationPage__header">One more thing(actually two)</h2>
        <OrderedList list={explanations} />
      </div>
      <div className="link-group ExplanationPage__link-group">
        <Link
          onClick={() => {}}
          className="link link--shifted-down"
          to={props.routes.pathToSignUpPage}
        >
          <button
            className="button button--fullwidth border--sm-border-radius ExplanationPage__button"
          >
            Join Geo Referral
          </button>
        </Link>
        <Link
          to={props.routes.pathToLoginPage}
          className="link ExplanationPage__link"
        >
          Already have an account? Log in
        </Link>
      </div>
    </section>
  );
}
