module.exports = {
    "extends": "airbnb",
    "rules": {
        "react/jsx-filename-extension": 0,
        "jsx-a11y/anchor-is-valid": 0,
        "react/prop-types": 0,
    },
    "env": {
        "browser": true
    }
};